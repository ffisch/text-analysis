# Text Analysis

- https://programminghistorian.org/en/lessons/cleaning-ocrd-text-with-regular-expressions
- https://programminghistorian.org/en/lessons/naive-bayesian
- https://programminghistorian.org/en/lessons/sentiment-analysis
- https://programminghistorian.org/en/lessons/introduction-to-stylometry-with-python#fn:28
- http://www.aicbt.com/authorship-attribution/
- https://learning.oreilly.com/library/view/natural-language-processing/9781501510427/e9781501510427_c20.html
- entity linking

# Python Network Visualization
- Networkx
- igraph
- pyvis
- graphtool

# Audio
- https://programminghistorian.org/en/lessons/sonification
- https://waxy.org/2015/12/if_drake_was_born_a_piano/

